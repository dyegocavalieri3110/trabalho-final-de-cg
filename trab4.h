#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <math.h>
#include "tinyxml2.h"
#include <vector>
#include <cmath>

using namespace std;
using namespace tinyxml2;

void read_xml (char* Arq);

void read_svg (char* Arq);

void ajeitay();

void calculosDecolagem();

float distancia2pontos (float x1, float y1, float x2, float y2);

void colidindo();

int colidindov2();

int maisDistanteVector();

class Circulo 
{ 
    public: 
    float centro_x, centro_y, raio, corR, corG, corB;
}; 

class Arena : public Circulo {

};

class Jogador : public Circulo {
    public:
    float velocidade;
    float ang, angCanhao, rodaHelice;
    int topoX,topoY;
    void MoveJogador (float speed, GLdouble timeDiff);
    void MoveHelice (float speed, GLdouble timeDiff);
    void MoveHeliceDecolagem (GLdouble S, GLdouble timeDiff, GLdouble inicioPista, GLdouble fimPista);
    void DesenhaAviao();
    void anguloCanhao();
    void IncrementaAng(GLdouble timeDiff);
    void DecrementaAng(GLdouble timeDiff);
    void IncrementaVel(GLdouble timeDiff);
    void DecrementaVel(GLdouble timeDiff);
    void Teleporta();
    void pontoTopo();
};

class Tiro : public Circulo {
    public:
    float velocidade, ang;
};

class Bomba : public Circulo {
    public:
    float velocidade, ang;
    GLdouble createdTime;
    void baseMorta();
};

class InimigoTerrestre : public Circulo {

};

class InimigoAereo : public Circulo {
    public:
    float ang, rodaHelice, velocidade, centro_x_inicial, centro_y_inicial, freqTiro, tirosDados;
    GLdouble refTime;
    int flagTeleporte;
    void MoveInimigo (float speed, GLdouble timeDiff);
    void MoveHelice (float speed, GLdouble timeDiff);
    void IncrementaAng(GLdouble timeDiff);
    void DecrementaAng(GLdouble timeDiff);
    void TeleportaInimigo();
    void RandomizaAng(GLdouble time);
    void Atira(GLdouble time);
    void ContaTiros(GLdouble time);
};

class Pista {
    public: 
    float x1,x2,y1,y2;
    string colorInfo;
    float corR, corG, corB;
};