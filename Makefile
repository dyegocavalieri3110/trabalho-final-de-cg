
all: g++

g++: trab4.cpp tinyxml2.cpp
	g++ trab4.cpp tinyxml2.cpp -lglut -lGLU -lGL -lm -std=c++11 -o trabalhocg

clean:
	@rm -f trabalhocg
