#include "trab4.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <math.h>
#include "tinyxml2.h"
#include <vector>
#include <cmath>
#include <ctype.h>
#include <sstream>
#include <unistd.h>

using namespace std;
using namespace tinyxml2;

//Declaração de variáveis globais
char** argv2;
// Text variable
static char str[2000];
void * font = GLUT_BITMAP_9_BY_15;
void * font2 = GLUT_BITMAP_HELVETICA_18;
//void * font3 = GLUT_BITMAP_TIMES_ROMAN_24;
string nomeArq;
string extArq;
string arenaArq;
string caminhoArq;
string velocidadeString;
string velocidadeTiroString;
int totalBases;
int restantes;
float velocidade;
float velocidadeT;
float velocidadeInimigo;
float velocidadeTiroInimigo;
float freqTiroInimigo;
int colisaoVoador = 0;
vector <InimigoAereo> vectorInimigosAereos;
vector <InimigoAereo> initialVectorInimigosAereos;
vector <InimigoTerrestre> vectorInimigosTerrestres;
vector <InimigoTerrestre> initialVectorInimigosTerrestres;
vector <Tiro> vectorTiros;
vector <Tiro> vectorTirosInimigos;
vector <Bomba> vectorBombas;
vector <int> arenaPointsX;
vector <int> arenaPointsY;
Arena arena;
Jogador jogador;
Pista pista;
int keyStatus[256];
int decolou=0;
int decolando=0;
float velocidadeBase = 0;
float copiaVelocidadeBase = 0;
float velocidadeBaseInimigo = 0;
GLdouble inicioDecolagem=0;
GLdouble tempoPercorridoDecolagem=0;
static  GLdouble previousTime = 0;
        GLdouble currentTime;
        GLdouble timeDiference;
        GLdouble sumTimeDiference = 0;
float ax;
float ay;
float RaioInicialDoJogador=0;
float AnguloInicial=0;
int passiveX;
int passiveY;
int clickX;
int clickY;
int flagTeleporte=0;

int largura, altura;

//lendo o xml
void read_xml (char* Arq){

    XMLDocument xml;
   if( xml.LoadFile(strcat(Arq,"config.xml")) != XML_SUCCESS ){
       cout << "Arquivo xml não encontrado" << endl;
       exit (1);
   }

    XMLNode* Raiz = xml.FirstChild();

    XMLElement* Elemento = Raiz->FirstChildElement("aplicacao");

    Elemento = Raiz->FirstChildElement("arquivoDaArena");

    Elemento = Elemento->FirstChildElement("nome");
    nomeArq = Elemento->GetText();
    Elemento = Raiz->FirstChildElement("arquivoDaArena");
    Elemento = Elemento->FirstChildElement("tipo");
    extArq = Elemento->GetText();
    Elemento = Raiz->FirstChildElement("arquivoDaArena");
    Elemento = Elemento->FirstChildElement("caminho");
    caminhoArq = Elemento->GetText();

    arenaArq = caminhoArq + nomeArq + "." + extArq;

    velocidadeString = Raiz->FirstChildElement("jogador")->Attribute("vel");
    velocidadeTiroString = Raiz->FirstChildElement("jogador")->Attribute("velTiro");
    
    string velocidadeInimigoString = Raiz->FirstChildElement("inimigo")->Attribute("vel");
    string velocidadeTiroInimigoString = Raiz->FirstChildElement("inimigo")->Attribute("velTiro");
    string freqTiroInimigoString = Raiz->FirstChildElement("inimigo")->Attribute("freqTiro");

    velocidadeInimigo = stof(velocidadeInimigoString);
    velocidadeTiroInimigo = stof(velocidadeTiroInimigoString);
    freqTiroInimigo = stof(freqTiroInimigoString);

    velocidade = stof(velocidadeString);
    velocidadeT = stof(velocidadeTiroString);

    cout << "Mult Vel Avião: " << velocidade << endl;
    cout << "Mult Vel Tiro: " << velocidadeT << endl;
    cout << "Mult Vel Tiro Inimigo: " << velocidadeTiroInimigo << endl;
    cout << "Mult Vel Inimigo: " << velocidadeInimigo << endl;
    cout << "Freq Tiro Inimigo: " << freqTiroInimigo << endl;

    char ArquivoSVG[arenaArq.size()+1];

    strcpy(ArquivoSVG, arenaArq.c_str());

    xml.Clear();

    read_svg(ArquivoSVG);

    
}

//lendo o svg
void read_svg (char* Arq){

    XMLDocument svg;

   if( svg.LoadFile(Arq) != XML_SUCCESS ){
       cout << "Arquivo não svg encontrado" << endl;
       exit (1);
   }

    XMLNode* Raiz =  svg.FirstChildElement("svg");

    for (XMLElement* child = Raiz->FirstChildElement(); child != NULL; child = child->NextSiblingElement("circle")) {
        int cx, cy, r, id;
        const char * fill;
        InimigoTerrestre inimigoTerrestre;
        InimigoAereo inimigoAereo;
        child->QueryIntAttribute("cx",&cx);
        child->QueryIntAttribute("cy",&cy);
        child->QueryIntAttribute("r",&r);
        child->QueryStringAttribute("fill",&fill);
        child->QueryIntAttribute("id",&id);

            if(strcmp(fill,"blue")==0){
                arena.corB = 1;
                arena.corG = 0;
                arena.corR = 0;            
                arena.centro_x = cx;
                arena.centro_y = cy;
                arena.raio = r;
                //cout << "passou ARENA" << endl;
            } else if(strcmp(fill,"red")==0){
                inimigoAereo.corR = 1;
                inimigoAereo.corG = 0;
                inimigoAereo.corB = 0;
                inimigoAereo.centro_x = cx;
                inimigoAereo.centro_y = cy;
                inimigoAereo.centro_x_inicial = cx;
                inimigoAereo.centro_y_inicial = cy;
                inimigoAereo.raio = r;
                inimigoAereo.velocidade = velocidadeInimigo;
                inimigoAereo.ang = -90;
                inimigoAereo.flagTeleporte = 0;
                inimigoAereo.freqTiro = freqTiroInimigo;
                inimigoAereo.refTime = glutGet(GLUT_ELAPSED_TIME);
                inimigoAereo.tirosDados = 0;
                vectorInimigosAereos.push_back(inimigoAereo);
                initialVectorInimigosAereos.push_back(inimigoAereo);
                //cout<<"passou aqui AER"<< endl;
            } else if(strcmp(fill,"orange")==0){
                inimigoTerrestre.corR = 1;
                inimigoTerrestre.corG = 0.5;
                inimigoTerrestre.corB = 0;
                inimigoTerrestre.centro_x = cx;
                inimigoTerrestre.centro_y = cy;
                inimigoTerrestre.raio = r;
                vectorInimigosTerrestres.push_back(inimigoTerrestre);
                initialVectorInimigosTerrestres.push_back(inimigoTerrestre);
                //cout<<"passou aqui TERR"<< endl;
            } else if(strcmp(fill,"green")==0){
                jogador.corR = 0;
                jogador.corG = 1;
                jogador.corB = 0;
                jogador.centro_x = cx;
                jogador.centro_y = cy;
                jogador.raio = r;
                //jogador.raio = 50;
                RaioInicialDoJogador = r;
                jogador.velocidade = velocidade;
                //cout << "passou jogador" << endl;
            }

        //cout << "loop" << endl;
    }
    XMLElement* child = Raiz->FirstChildElement("line");
    int x1,y1,x2,y2;
    const char* style;
    child->QueryIntAttribute("x1",&x1);
    child->QueryIntAttribute("x2",&x2);
    child->QueryIntAttribute("y1",&y1);
    child->QueryIntAttribute("y2",&y2);
    child->QueryStringAttribute("style",&style);

    pista.x1=x1;
    pista.x2=x2;
    pista.y1=y1;
    pista.y2=y2;

    totalBases = initialVectorInimigosTerrestres.size();
    ajeitay();
    largura = altura = arena.raio*2;
    svg.Clear();
}

//ajeitando o eixo y que cresce pra baixo por padrão no opengl
void ajeitay(){

    jogador.centro_y = arena.centro_y+arena.raio-jogador.centro_y;
    
    for (int j = 0; j < vectorInimigosAereos.size(); j++) {
        vectorInimigosAereos[j].centro_y = arena.centro_y + arena.raio - vectorInimigosAereos[j].centro_y;
        vectorInimigosAereos[j].centro_y_inicial = arena.centro_y + arena.raio - vectorInimigosAereos[j].centro_y_inicial;
    }
    
    for (int j = 0; j < vectorInimigosTerrestres.size(); j++) {
        vectorInimigosTerrestres[j].centro_y = arena.centro_y + arena.raio - vectorInimigosTerrestres[j].centro_y;
    }

    for (int j = 0; j < initialVectorInimigosAereos.size(); j++) {
        initialVectorInimigosAereos[j].centro_y = arena.centro_y + arena.raio - initialVectorInimigosAereos[j].centro_y;
        initialVectorInimigosAereos[j].centro_y_inicial = arena.centro_y + arena.raio - initialVectorInimigosAereos[j].centro_y_inicial;
    }
    
    for (int j = 0; j < initialVectorInimigosTerrestres.size(); j++) {
        initialVectorInimigosTerrestres[j].centro_y = arena.centro_y + arena.raio - initialVectorInimigosTerrestres[j].centro_y;
    }


    pista.y1 = arena.centro_y + arena.raio - pista.y1;

    pista.y2 = arena.centro_y + arena.raio - pista.y2;

    arena.centro_y = arena.centro_y + arena.raio - arena.centro_y;
  
}

void keyUp(unsigned char key, int x, int y)
{
	keyStatus[key] = 0;
	glutPostRedisplay();
}

//na key Press estou tratando a tecla de decolagem
void keyPress(unsigned char key, int x, int y)
{
    keyStatus[key] = 1;
        if(keyStatus['u'] == 1 || keyStatus['U'] == 1 ){  
            if (decolando == 0){
                GLdouble aux = glutGet(GLUT_ELAPSED_TIME);
                inicioDecolagem = aux;
                calculosDecolagem();
            }
        decolando = 1;
        glutPostRedisplay();
        }
}

//herança do trabalho 1
float distancia2pontos (float x1, float y1, float x2, float y2){
	return sqrt(pow((x1 - x2),2)+ pow((y1 - y2),2));
}

//verifica se a proxima posição terá colisão
int vaiBater(int novoX, int novoY){
    int colisao=0;

        if(distancia2pontos(arena.centro_x,arena.centro_y,novoX,novoY)>(arena.raio)){
            colisao = 1;
        }

        return colisao;
}
//Mensagem final de Game Over ou Vitória
void PrintFinalMessage(GLfloat x, GLfloat y){

    if (colisaoVoador==1 && restantes!=0){
        glColor3f(1,0,0);
            char *tmpStr;
            sprintf(str, "GAME OVER! Press 'r' to try again!");

            glRasterPos2f(x, y);

            tmpStr = str;

            while( *tmpStr ){
                glutBitmapCharacter(font2, *tmpStr);
                tmpStr++;
            }
    }
       else if (restantes==0){
           glColor3f(0,1,0);
            char *tmpStr;
            sprintf(str, "YOU WON! Press 'r' to play again!");

            glRasterPos2f(x, y);

            tmpStr = str;

            while( *tmpStr ){
                glutBitmapCharacter(font2, *tmpStr);
                tmpStr++;
            }
    }
}

void PrintScore(GLfloat x, GLfloat y)
{
    glColor3f(0,1,0);

    char *tmpStr;
    int fromVector;

    int destruidas = (initialVectorInimigosTerrestres.size() - vectorInimigosTerrestres.size());
    restantes = (vectorInimigosTerrestres.size());
    sprintf(str, "Destroyed Bases: %d , %d Remaining", destruidas,restantes);

    glRasterPos2f(x, y);

    tmpStr = str;

    while( *tmpStr ){
        glutBitmapCharacter(font, *tmpStr);
        tmpStr++;
    }

}

void colisaoInimigoVoador(int novoX,int novoY){
    	for (int j = 0; j < vectorInimigosAereos.size(); j++) {
            if(distancia2pontos(vectorInimigosAereos[j].centro_x,vectorInimigosAereos[j].centro_y,novoX,novoY) < vectorInimigosAereos[j].raio + jogador.raio && colisaoVoador != 1){
                colisaoVoador = 1;
                cout << "Game Over! Você colidiu em um inimigo. Pressione r para reiniciar." << endl;
                break;
            }
	    } 
        for (int i = 0; i < vectorTirosInimigos.size(); i++) {
            if(distancia2pontos(vectorTirosInimigos[i].centro_x,vectorTirosInimigos[i].centro_y,novoX,novoY) < vectorTirosInimigos[i].raio + jogador.raio && colisaoVoador != 1){
                colisaoVoador = 1;
                cout << "Game Over! Você foi atingido por um tiro inimigo. Pressione r para reiniciar." << endl;
                break;
            }
        }
    }

void inimigoMorto (){
        	for (int j = 0; j < vectorInimigosAereos.size(); j++) {
                for(int i=0; i<vectorTiros.size();i++){
                    if(distancia2pontos(vectorInimigosAereos[j].centro_x,vectorInimigosAereos[j].centro_y,vectorTiros[i].centro_x,vectorTiros[i].centro_y)<vectorTiros[i].raio+vectorInimigosAereos[j].raio){
                        vectorInimigosAereos.erase(vectorInimigosAereos.begin() + j);
                    }
                }
	        } 
}

void Bomba::baseMorta (){
        for (int j = 0; j < vectorInimigosTerrestres.size(); j++) {     
            if(distancia2pontos(vectorInimigosTerrestres[j].centro_x,vectorInimigosTerrestres[j].centro_y,this->centro_x,this->centro_y)<this->raio+vectorInimigosTerrestres[j].raio){
                vectorInimigosTerrestres.erase(vectorInimigosTerrestres.begin() + j);
            }
        }
}

void desenhaCirculo(GLdouble raio, float R, float G, float B){
    	float angle, aux_xm, aux_ym;
        int flagSaveVector=0;
	GLfloat circle_points = 100;

    if (arena.raio == raio)
        flagSaveVector =1;

		glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(R, G, B);

		for (int i = 0; i < circle_points; i++){
			angle = 2*M_PI*i/circle_points;
			aux_xm = cosf(angle)*raio;
			aux_ym = sinf(angle)*raio;
			glVertex2f(aux_xm, aux_ym);
                if (flagSaveVector == 1){
                    arenaPointsX.push_back((aux_xm)+arena.centro_x);
                    arenaPointsY.push_back((aux_ym)+arena.centro_y);
                }
		}
		glEnd();
    glFlush();
}

void desenhaArena(){
    glPushMatrix();
        glTranslatef(arena.centro_x, arena.centro_y, 0);
        desenhaCirculo(arena.raio,arena.corR,arena.corG,arena.corB);
    glPopMatrix();

}

void desenhaElipseAviao(GLdouble raio, float R, float G, float B){
    	float angle, aux_xm, aux_ym;
	    GLfloat circle_points = 50;
		glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(R, G, B);

		for (int i = 0; i < circle_points; i++){
			angle = 2*M_PI*i/circle_points;
			aux_xm = cosf(angle)*(float)raio/4;
			aux_ym = sinf(angle)*(float)raio;
			glVertex2f(aux_xm, aux_ym);
		}
		glEnd();
}


void desenhaRetangulo (GLint height, GLint width, GLfloat R, GLfloat G, GLfloat B) {

	glColor3f(R,G,B);
	glBegin(GL_POLYGON);
		glVertex3f(-width/2.0,0.0,0.0);
		glVertex3f(width/2.0,0.0,0.0);
		glVertex3f(width/2.0,height,0.0);
		glVertex3f(-width/2.0,height,0.0);
	glEnd();

}

void desenhaQuadrilateroAE(GLfloat height, GLfloat width, GLfloat R, GLfloat G, GLfloat B){
    glColor3f(R,G,B);
    glBegin(GL_QUADS);
		glVertex3f(-width/2.0,0.0,0.0);
		glVertex3f(width/2.0,height/2,0.0);
		glVertex3f(width/2.0,height+height/2,0.0);
		glVertex3f(-width/2.0,height,0.0);

    glEnd();
}

void desenhaQuadrilateroAD(GLfloat height, GLfloat width, GLfloat R, GLfloat G, GLfloat B){
    glColor3f(R,G,B);
    glBegin(GL_QUADS);
		glVertex3f(-width/2.0,0.0,0.0);
		glVertex3f(width/2.0,-height/2,0.0);
		glVertex3f(width/2.0,height-height/2,0.0);
		glVertex3f(-width/2.0,height,0.0);
    glEnd();
}

void desenhaTrianguloE(GLfloat size, GLfloat R, GLfloat G, GLfloat B){
        glColor3f(R,G,B);
        glBegin(GL_TRIANGLES);
		glVertex3f(0,0,0.0);
		glVertex3f(size,0,0.0);
		glVertex3f(0,size/2,0.0);
    glEnd();
}

void desenhaTrianguloD(GLfloat size, GLfloat R, GLfloat G, GLfloat B){
        glColor3f(R,G,B);
        glBegin(GL_TRIANGLES);
		glVertex3f(0,0,0.0);
		glVertex3f(-size,0,0.0);
		glVertex3f(0,size/2,0.0);
    glEnd();
}

void limpaTiros() {

        for (int i = 0; i < vectorTiros.size(); i++) {
            if(abs(vectorTiros[i].centro_y) >= 5*abs(arena.centro_y)){
                vectorTiros.erase(vectorTiros.begin() + i);
            }
        }
}

void limpaTirosInimigos() {

        for (int i = 0; i < vectorTirosInimigos.size(); i++) {
            if(abs(vectorTirosInimigos[i].centro_y) >= 5*abs(arena.centro_y)){
                vectorTirosInimigos.erase(vectorTirosInimigos.begin() + i);
            }
        }
}

void Jogador::DesenhaAviao(){
    //Circulo de verificação de colisão
    glPushMatrix();
        glTranslatef(jogador.centro_x, jogador.centro_y, 0);
        glRotatef(jogador.ang,0.0,0.0,1.0);
/*
            glPushMatrix();
                desenhaCirculo(jogador.raio,0.0,0.0,0.5);
            glPopMatrix();*/
            //"Charuto" do avião
            glPushMatrix();
                desenhaElipseAviao(jogador.raio,jogador.corR,jogador.corG,jogador.corB);
            glPopMatrix();
            //Cockpit do avião
            glPushMatrix();
                glTranslatef(0, jogador.raio/2, 0);
                desenhaElipseAviao(jogador.raio/2,0.0,0.0,0.0);
            glPopMatrix();
            //Cauda do avião
            glPushMatrix();
                glTranslatef(0,-jogador.raio, 0);
                desenhaRetangulo(jogador.raio/2,jogador.raio/6,0,0,0);
            glPopMatrix();
            //Canhão do avião
            glPushMatrix();
                glTranslatef(0,jogador.raio,0);
                    if(colisaoVoador != 1)
                        glRotatef(jogador.angCanhao,0.0,0.0,1.0);
                desenhaRetangulo(jogador.raio/2,jogador.raio/6,0,0,0);
            glPopMatrix();
            //Asa Esquerda
            glPushMatrix();
                glTranslatef(-jogador.raio/1.8,-jogador.raio/2,0);
                desenhaQuadrilateroAE(jogador.raio/2,jogador.raio/1.7,0,0,0);
                    glPushMatrix();
                        glTranslatef(0,raio/4,0);
                        desenhaRetangulo(jogador.raio/2,jogador.raio/8,0,0,0);
                    glPopMatrix();
            glPopMatrix();
            //Asa Direita
            glPushMatrix();
                glTranslatef(jogador.raio/1.8,-jogador.raio/4,0);
                desenhaQuadrilateroAD(jogador.raio/2,jogador.raio/1.7,0,0,0);
                    glPushMatrix();
                        glTranslatef(0,raio/2,0);
                        desenhaRetangulo(-jogador.raio/2,-jogador.raio/8,0,0,0);
                    glPopMatrix();
            glPopMatrix();
            //Desenha Hélices da Asa Direita
                            glPushMatrix();
                                glTranslatef(raio/3,raio/4,0);
                                glRotatef(jogador.rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloE(jogador.raio/4,1,1,0);
                            glPopMatrix();
                            
                            glPushMatrix();
                                glTranslatef(raio/1.2,raio/4,0);
                                glRotatef(jogador.rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloD(jogador.raio/4,1,1,0);
                            glPopMatrix();

            //Desenha Hélices da Asa Esquerda
                            glPushMatrix();
                                glTranslatef(-raio/1.2,raio/4,0);
                                glRotatef(jogador.rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloE(jogador.raio/4,1,1,0);
                            glPopMatrix();

                            glPushMatrix();
                                glTranslatef(-raio/3,raio/4,0);
                                glRotatef(jogador.rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloD(jogador.raio/4,1,1,0);
                            glPopMatrix();

    glPopMatrix();
}

void desenhaAereos(){
    for (int i = 0; i < vectorInimigosAereos.size(); i++) {
        glPushMatrix();
            glTranslatef(vectorInimigosAereos[i].centro_x, vectorInimigosAereos[i].centro_y, 0);
            glRotatef(vectorInimigosAereos[i].ang,0.0,0.0,1.0);
            //desenhaCirculo(vectorInimigosAereos[i].raio, vectorInimigosAereos[i].corR, vectorInimigosAereos[i].corG, vectorInimigosAereos[i].corB);
            //Charuto
            glPushMatrix();
                desenhaElipseAviao(vectorInimigosAereos[i].raio,vectorInimigosAereos[i].corR,vectorInimigosAereos[i].corG,vectorInimigosAereos[i].corB);
            glPopMatrix();
            //Cockpit
            glPushMatrix();
                glTranslatef(0,vectorInimigosAereos[i].raio/2,0);
                desenhaElipseAviao(vectorInimigosAereos[i].raio/2,0,0,0);
            glPopMatrix();
            //Cauda
            glPushMatrix();
                glTranslatef(0,-vectorInimigosAereos[i].raio,0);
                desenhaRetangulo(vectorInimigosAereos[i].raio/2,vectorInimigosAereos[i].raio/6,0,0,0);
            glPopMatrix();
            //Canhão
            glPushMatrix();
                glTranslatef(0,vectorInimigosAereos[i].raio,0);
                desenhaRetangulo(vectorInimigosAereos[i].raio/2,vectorInimigosAereos[i].raio/6,0,0,0);
            glPopMatrix();
            //Asa Esquerda
            glPushMatrix();
                glTranslatef(-vectorInimigosAereos[i].raio/1.8,-vectorInimigosAereos[i].raio/2,0);
                desenhaQuadrilateroAE(vectorInimigosAereos[i].raio/2,vectorInimigosAereos[i].raio/1.7,0,0,0);
                    glPushMatrix();
                        glTranslatef(0,vectorInimigosAereos[i].raio/4,0);
                        desenhaRetangulo(vectorInimigosAereos[i].raio/2,vectorInimigosAereos[i].raio/8,0,0,0);
                    glPopMatrix();
            glPopMatrix();
            //Asa Direita
            glPushMatrix();
                glTranslatef(vectorInimigosAereos[i].raio/1.8,-vectorInimigosAereos[i].raio/4,0);
                desenhaQuadrilateroAD(vectorInimigosAereos[i].raio/2,vectorInimigosAereos[i].raio/1.7,0,0,0);
                    glPushMatrix();
                        glTranslatef(0,vectorInimigosAereos[i].raio/2,0);
                        desenhaRetangulo(-vectorInimigosAereos[i].raio/2,-vectorInimigosAereos[i].raio/8,0,0,0);
                    glPopMatrix();
            glPopMatrix();
            //Desenha Hélices da Asa Direita
                            glPushMatrix();
                                glTranslatef(vectorInimigosAereos[i].raio/3,vectorInimigosAereos[i].raio/4,0);
                                glRotatef(vectorInimigosAereos[i].rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloE(vectorInimigosAereos[i].raio/4,1,1,0);
                            glPopMatrix();
                            
                            glPushMatrix();
                                glTranslatef(vectorInimigosAereos[i].raio/1.2,vectorInimigosAereos[i].raio/4,0);
                                glRotatef(vectorInimigosAereos[i].rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloD(vectorInimigosAereos[i].raio/4,1,1,0);
                            glPopMatrix();
             //Desenha Hélices da Asa Esquerda
                            glPushMatrix();
                                glTranslatef(-vectorInimigosAereos[i].raio/1.2,vectorInimigosAereos[i].raio/4,0);
                                glRotatef(vectorInimigosAereos[i].rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloE(vectorInimigosAereos[i].raio/4,1,1,0);
                            glPopMatrix();

                            glPushMatrix();
                                glTranslatef(-vectorInimigosAereos[i].raio/3,vectorInimigosAereos[i].raio/4,0);
                                glRotatef(vectorInimigosAereos[i].rodaHelice,1.0,0.0,0.0);
                                desenhaTrianguloD(vectorInimigosAereos[i].raio/4,1,1,0);
                            glPopMatrix();
        glPopMatrix();
    }
}

void desenhaTerrestres(){
    for (int i = 0; i < vectorInimigosTerrestres.size(); i++) {
        glPushMatrix();
            glTranslatef(vectorInimigosTerrestres[i].centro_x, vectorInimigosTerrestres[i].centro_y, 0);
            desenhaCirculo(vectorInimigosTerrestres[i].raio, vectorInimigosTerrestres[i].corR, vectorInimigosTerrestres[i].corG, vectorInimigosTerrestres[i].corB);
            desenhaCirculo(vectorInimigosTerrestres[i].raio/1.5, 0, 0, 0);
            desenhaCirculo(vectorInimigosTerrestres[i].raio/4, vectorInimigosTerrestres[i].corR, vectorInimigosTerrestres[i].corG, vectorInimigosTerrestres[i].corB);
        glPopMatrix();
    }
}

void desenhaTiros(){
    for (int i = 0; i < vectorTiros.size(); i++) {
        glPushMatrix();
            glTranslatef(vectorTiros[i].centro_x, vectorTiros[i].centro_y, 0);
            desenhaCirculo(vectorTiros[i].raio, vectorTiros[i].corR, vectorTiros[i].corG, vectorTiros[i].corB);
        glPopMatrix();
    }
}

void desenhaTirosInimigos(){
    for (int i = 0; i < vectorTirosInimigos.size(); i++) {
        glPushMatrix();
            glTranslatef(vectorTirosInimigos[i].centro_x, vectorTirosInimigos[i].centro_y, 0);
            desenhaCirculo(vectorTirosInimigos[i].raio, vectorTirosInimigos[i].corR, vectorTirosInimigos[i].corG, vectorTirosInimigos[i].corB);
        glPopMatrix();
    }
}

void desenhaBombas(){
    for (int i = 0; i < vectorBombas.size(); i++) {
        glPushMatrix();
            glTranslatef(vectorBombas[i].centro_x, vectorBombas[i].centro_y, 0);
            desenhaCirculo(vectorBombas[i].raio, vectorBombas[i].corR, vectorBombas[i].corG, vectorBombas[i].corB);
        glPopMatrix();
    }
}

void diminuiRaioBombas(GLdouble tempo){
        for (int i = 0; i < vectorBombas.size(); i++) {
            if(tempo - vectorBombas[i].createdTime >= 2000){
                vectorBombas[i].baseMorta();
                vectorBombas.erase(vectorBombas.begin() + i);
            }
            vectorBombas[i].raio = jogador.raio/4 - (jogador.raio/8)*((tempo - vectorBombas[i].createdTime)/2000);
            //cout << ((tempo - vectorBombas[i].createdTime)/4000) << endl;
        }
}

void TeleporteJogador() {

    //Deprecated Teleport 1
  /*  // 1/(m^2+1)
    double a = jogador.topoY, b = jogador.topoX, c = arena.centro_y, d = arena.centro_x, m = (tan(jogador.ang)*M_PI/180), r = arena.raio+jogador.raio;
    double mPart = (1/(m*m+1));
    // Raiz Enorme
    double rootPart = sqrt(-a*a+2*a*b*m+2*a*c-2*a*d*m-b*b*m*m-2*b*c*m+2*b*d*m*m-c*c+2*c*d*m-d*d*m*m+m*m*r*r+r*r);
    // Depois da raiz, no calc de X
    double afterRootX = (-a*m+b*m*m+c*m+d);
    // Depois da raiz, no calc de Y
    double afterRootY = (a-b*m+c*m*m+d*m);
    //Finalmente, X1, Y1, X2, Y2
    double x1 = mPart*(-(rootPart)+afterRootX);
    double y1 = mPart*((-m)*(rootPart)+afterRootY);

    double x2 = mPart*((rootPart)+afterRootX);
    double y2 = mPart*(m*(rootPart)+afterRootY);


    if (isnan(x1)!=1 && isnan(x2)!=1 && isnan(y1)!=1 && isnan(y2)!=1){
        if (distancia2pontos(x1,y1,jogador.centro_x,jogador.centro_y)>distancia2pontos(x2,y2,jogador.centro_x,jogador.centro_y)){
            jogador.centro_x = x1;
            jogador.centro_y = y1;
                cout << "x1: " << x1 << endl;
                cout << "y1: " << y1 << endl;
            //jogador.ang = jogador.ang+180;
        } else {
            jogador.centro_x = x2;
            jogador.centro_y = y2;
                cout << "x2: " << x2 << endl;
                cout << "y2: " << y2 << endl;
            //jogador.ang = jogador.ang+180;     
        }
    } else if (jogador.topoY > arena.centro_y) {
        jogador.centro_y = arena.centro_y - arena.raio;
        jogador.centro_x = arena.centro_x;
    } else {
        jogador.centro_y = arena.centro_y + arena.raio;
        jogador.centro_x = arena.centro_x;
    }*/
    //Deprecated Teleport 2
   /* int maiorI = maisDistanteVector();
    jogador.centro_x = arenaPointsX[maiorI];
    jogador.centro_y = arenaPointsY[maiorI];
    cout << "teleport X: " << arenaPointsX[maiorI] << endl;
    cout << "teleport Y: " << arenaPointsY[maiorI] << endl;*/

        double copiaX;
        double copiaY;
        double inicialX;
        double inicialY;
        double finalX;
        double finalY;
        double copiaAng;
        int beenHereDoneThat=0;

        copiaX = jogador.centro_x;
        copiaY = jogador.centro_y;
        copiaAng = jogador.ang;


            while(true){
                //O avião está em estado de colisão, esse while o tira desse estado
                while(beenHereDoneThat==0){
                    copiaX = copiaX + 0.1 * sinf(copiaAng*(2*M_PI)/360);
                    copiaY = copiaY - 0.1 * cosf(copiaAng*(2*M_PI)/360);
                    if(vaiBater(copiaX,copiaY)==0){
                       // cout << "Ponto onde não bate mais!" << endl;
                       // cout << copiaX << " " << copiaY << endl;
                        beenHereDoneThat=1;
                        inicialX = copiaX;
                        inicialY = copiaY;
                    }
                }

                copiaX = copiaX + 0.1 * sinf(copiaAng*(2*M_PI)/360);
                copiaY = copiaY - 0.1 * cosf(copiaAng*(2*M_PI)/360);
                
                //aqui o avião colidiu de novo, queremos teleportar pra esse ponto
                if(vaiBater(copiaX,copiaY)==1){
                        //cout << "Segunda batida, atribuindo esses pontos ao Aviao!" << endl;
                        //cout << copiaX << " " << copiaY << endl;
                    //porém, se a dist for muito curta, o avião pode ficar preso, se teleportando pra sempre
                    //por isso se o usuário entrar com um ângulo muito próximo do angulo do círculo, causando
                    //esse problema, teleportamos para o centro da arena pra evitar travamentos
                    if(abs(inicialX - copiaX) < 10){
                        jogador.centro_x = arena.centro_x;
                        jogador.centro_y = arena.centro_y;
                        cout << "Contingency Teleport" << endl;
                        break;
                    } else {
                    jogador.centro_x = copiaX;
                    jogador.centro_y = copiaY;
                    break;
                    }
                }
            }
}

void desenhaPista(){
    glBegin(GL_LINES);
    glColor3f(0.0,0.0,0.0);
        glVertex2f(pista.x1, pista.y1);
        glVertex2f(pista.x2, pista.y2);
    glEnd();
}

void display(void){
	glClear(GL_COLOR_BUFFER_BIT);
    desenhaArena();
    desenhaTerrestres();
    desenhaPista();
    desenhaBombas();
    desenhaTiros();
    desenhaTirosInimigos();
    jogador.DesenhaAviao();
    desenhaAereos();
    PrintScore(arena.centro_x, arena.centro_y+arena.raio-10);
    PrintFinalMessage((arena.centro_x+arena.raio)/2,arena.centro_y);
	glutSwapBuffers();
}

void setaAnguloInicial(){
    
jogador.ang = -90;
        jogador.ang = jogador.ang + atan2((pista.y2-pista.y1),(pista.x2-pista.x1))*180/M_PI;
        AnguloInicial = jogador.ang;
        cout << "angulo inicial: " << jogador.ang << endl;
}

void calculosVfParaInimigo(){

        float Sx=0;
        float Sy=0;
        float v0x=0;
        float v0y=0;
        float vx=0;
        float vy=0;
            
            ax = 2*abs(pista.x2-pista.x1)/pow(4000,2);
            ay = 2*abs(pista.y1-pista.y2)/pow(4000,2);
            cout << "acelerações: " << ax << " " << ay << endl;
            float vfx, vfy;
            vfx = ax*4000;
            vfy = ay*4000;
            cout << "velocidades finais: " << vfx << " " << vfy << endl;
            float vf;
            vf = sqrt(pow(vfx,2)+pow(vfy,2));
            velocidadeBaseInimigo = vf;
}

void init (void){
    glClearColor(0.0,0.0,0.0,0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //configurando a camera pro padrão pedido
    glOrtho(arena.centro_x-arena.raio, arena.centro_x+arena.raio, arena.centro_y-arena.raio, arena.centro_y+arena.raio, -1, 1);

    setaAnguloInicial();
    calculosVfParaInimigo();
}

void Jogador::MoveHeliceDecolagem (GLdouble S, GLdouble timeDiff, GLdouble inicioPista, GLdouble fimPista){
        GLdouble vel = (abs(S)-abs(fimPista))/(abs(fimPista));
        jogador.rodaHelice += (vel/100 * timeDiff*360)/(2*M_PI*30);
}

void Jogador::MoveHelice(float speed, GLdouble timeDiff){
    jogador.rodaHelice += (10*speed * velocidadeBase * timeDiff*360)/(2*M_PI*30);
}

void InimigoAereo::MoveHelice(float speed, GLdouble timeDiff){
    this->rodaHelice += (10*speed * velocidadeBase * timeDiff*360)/(2*M_PI*30);
}

void Jogador::MoveJogador(float speed, GLdouble timeDiff){
    jogador.centro_x = jogador.centro_x - speed * velocidadeBase * timeDiff * sinf(jogador.ang*(2*M_PI)/360);
    jogador.centro_y = jogador.centro_y + speed * velocidadeBase * timeDiff * cosf(jogador.ang*(2*M_PI)/360);
}

void moveTiro(float speed, GLdouble timeDiff){
    for (int i = 0; i < vectorTiros.size(); i++) {
        vectorTiros[i].centro_x = vectorTiros[i].centro_x - speed * velocidadeBase * timeDiff * sinf(vectorTiros[i].ang*(2*M_PI)/360);
        vectorTiros[i].centro_y = vectorTiros[i].centro_y + speed * velocidadeBase * timeDiff * cosf(vectorTiros[i].ang*(2*M_PI)/360);
    }
}

void moveTiroInimigo(float speed, GLdouble timeDiff){
    for (int i = 0; i < vectorTirosInimigos.size(); i++) {
        vectorTirosInimigos[i].centro_x = vectorTirosInimigos[i].centro_x - speed * velocidadeBaseInimigo * timeDiff * sinf(vectorTirosInimigos[i].ang*(2*M_PI)/360);
        vectorTirosInimigos[i].centro_y = vectorTirosInimigos[i].centro_y + speed * velocidadeBaseInimigo * timeDiff * cosf(vectorTirosInimigos[i].ang*(2*M_PI)/360);
    }
}

void InimigoAereo::MoveInimigo(float speed, GLdouble timeDiff){
    this->centro_x -= speed*velocidadeBaseInimigo*timeDiff*sinf(this->ang*(2*M_PI)/360);
    this->centro_y += speed*velocidadeBaseInimigo*timeDiff*cosf(this->ang*(2*M_PI)/360);
}

void moveBomba(float speed, GLdouble timeDiff){
    for (int i = 0; i < vectorBombas.size(); i++) {
        vectorBombas[i].centro_x = vectorBombas[i].centro_x - speed * velocidadeBase * timeDiff * sinf(vectorBombas[i].ang*(2*M_PI)/360);
        vectorBombas[i].centro_y = vectorBombas[i].centro_y + speed * velocidadeBase * timeDiff * cosf(vectorBombas[i].ang*(2*M_PI)/360);
    }
}


int maisDistanteVector(){
    int maiorDist=0;
    int maiorI =0;
    for (int i = 0; i < arenaPointsX.size(); i++) {
        int distAtual = distancia2pontos(jogador.centro_x,jogador.centro_y,arenaPointsX[i],arenaPointsY[i]);
        if (distAtual>=maiorDist){
            maiorDist = distAtual;
            maiorI = i;
        }
    }
    return maiorI;
}

int maisDistanteVectorInimigo(InimigoAereo ia){
    int maiorDist=0;
    int maiorI =0;
    for (int i = 0; i < arenaPointsX.size(); i++) {
        int distAtual = distancia2pontos(ia.centro_x,ia.centro_y,arenaPointsX[i],arenaPointsY[i]);
        if (distAtual>=maiorDist){
            maiorDist = distAtual;
            maiorI = i;
        }
    }
    return maiorI;
}

//calculos de velocidade, aceleração etc
void calculosDecolagem(){
    decolando = 1;

        float Sx=0;
        float Sy=0;
        float v0x=0;
        float v0y=0;
        float vx=0;
        float vy=0;
            
            ax = 2*abs(pista.x2-pista.x1)/pow(4000,2);
            ay = 2*abs(pista.y1-pista.y2)/pow(4000,2);
            cout << "acelerações: " << ax << " " << ay << endl;
            float vfx, vfy;
            vfx = ax*4000;
            vfy = ay*4000;
            cout << "velocidades finais: " << vfx << " " << vfy << endl;
            float vf;
            vf = sqrt(pow(vfx,2)+pow(vfy,2));
            velocidadeBase = vf;
            copiaVelocidadeBase = vf;
            cout << "velocidade final resultante: " << vf << endl;
}

//funcao decola, aumenta o raio e movimenta o jogador na pista, usando os valores
//calculados anteriormente 
void decola(GLdouble currentTime){
    if(currentTime - inicioDecolagem > 4000){
        decolou = 1;
        return;
    }

    GLdouble tempo = (currentTime - inicioDecolagem);
    GLdouble Sx = jogador.centro_x;
    GLdouble Sy = jogador.centro_y;
    //caso=1 -> x cresce pra cima, y cresce pra cima
    //caso=2 -> x cresce pra baixo, y cresce pra cima
    //caso=3 -> x cresce pra cima, y cresce pra baixo
    //caso=4 -> x cresce pra baixo, y cresce pra baixo

    int caso=0;

    if(pista.x1 - pista.x2 < 0 && pista.y1 - pista.y2 < 0){    
        Sx = pista.x1 + ax*pow(tempo,2)/2;
        Sy = pista.y1 + ay*pow(tempo,2)/2;
        caso=1;
    } else if (pista.x1 - pista.x2 > 0 && pista.y1 - pista.y2 < 0){ 
        Sx = pista.x1 - ax*pow(tempo,2)/2;
        Sy = pista.y1 + ay*pow(tempo,2)/2;
        caso=2;
    } else if (pista.x1 - pista.x2 < 0 && pista.y1 - pista.y2 > 0){ 
        Sx = pista.x1 + ax*pow(tempo,2)/2;
        Sy = pista.y1 - ay*pow(tempo,2)/2;
        caso=3;;
    } else {
        Sx = pista.x1 - ax*pow(tempo,2)/2;
        Sy = pista.y1 - ay*pow(tempo,2)/2;
        caso=4;
    }
    jogador.centro_x = Sx;
    jogador.centro_y = Sy;

    GLdouble MeioDaPistaX = abs(pista.x1+pista.x2)/2;
    GLdouble MeioDaPistaY = abs(pista.y1+pista.y2)/2;

    int useY = 0;

    if (abs(pista.x1 - pista.x2) < abs(pista.y1 - pista.y2)){}
        useY = 1;
        

    if(caso ==1){
            if( (Sx) > (MeioDaPistaX) && (Sy) > (MeioDaPistaY) ) {
                if(useY == 1){
                    GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sy)-abs(MeioDaPistaY))/(abs(pista.y2)-abs(MeioDaPistaY));
                    jogador.raio = raioFinal;
                } else {
                GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sx)-abs(MeioDaPistaX))/(abs(pista.x2)-abs(MeioDaPistaX));
                jogador.raio = raioFinal;
            }
        }
    } else if (caso == 2) {
            if( (Sx) < (MeioDaPistaX) && (Sy) > (MeioDaPistaY) ) {
                if(useY == 1){
                    GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sy)-abs(MeioDaPistaY))/(abs(pista.y2)-abs(MeioDaPistaY));
                    jogador.raio = raioFinal;
                } else {
                GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sx)-abs(MeioDaPistaX))/(abs(pista.x2)-abs(MeioDaPistaX));
                jogador.raio = raioFinal;
            }
        }
    } else if (caso == 3) {
            if( (Sx) > (MeioDaPistaX) && (Sy) < (MeioDaPistaY) ) {
                if(useY == 1){
                    GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sy)-abs(MeioDaPistaY))/(abs(pista.y2)-abs(MeioDaPistaY));
                    jogador.raio = raioFinal;
                } else {
                GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sx)-abs(MeioDaPistaX))/(abs(pista.x2)-abs(MeioDaPistaX));
                jogador.raio = raioFinal;
            }
        }
    } else if (caso == 4) {
            if( (Sx) < (MeioDaPistaX) && (Sy) < (MeioDaPistaY) ) {
                if(useY == 1){
                    GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sy)-abs(MeioDaPistaY))/(abs(pista.y2)-abs(MeioDaPistaY));
                    jogador.raio = raioFinal;
                } else {
                GLdouble raioFinal = RaioInicialDoJogador + RaioInicialDoJogador*(abs(Sx)-abs(MeioDaPistaX))/(abs(pista.x2)-abs(MeioDaPistaX));
                jogador.raio = raioFinal;
            }
        }
    }
    if(useY == 0){
        jogador.MoveHeliceDecolagem(Sx,tempo,pista.x1,pista.x2);
    } else {
        jogador.MoveHeliceDecolagem(Sy,tempo,pista.y1,pista.y2);
    }

}

void Jogador::IncrementaAng(GLdouble timeDiff) {
    jogador.ang = jogador.ang + 0.1*timeDiff;
}

void InimigoAereo::IncrementaAng(GLdouble timeDiff) {
    this->ang = this->ang + 0.001*timeDiff;
}

void InimigoAereo::DecrementaAng(GLdouble timeDiff) {
    this->ang = this->ang - 0.001*timeDiff;
}

void Jogador::DecrementaAng(GLdouble timeDiff) {
    jogador.ang = jogador.ang - 0.1*timeDiff;
}

void Jogador::IncrementaVel(GLdouble timeDiff) {
    jogador.velocidade = jogador.velocidade + 0.001*timeDiff;
}

void Jogador::DecrementaVel(GLdouble timeDiff) {
    if (jogador.velocidade > 0.4)
        jogador.velocidade = jogador.velocidade - 0.001*timeDiff;
}

void restartGame(){
            jogador.centro_x = pista.x1;
            jogador.centro_y = pista.y1;
            setaAnguloInicial();
            jogador.raio = RaioInicialDoJogador;
            jogador.velocidade = velocidade;
            velocidadeBase = 0;
            colisaoVoador = 0;
            decolando = 0;
            decolou = 0;
            vectorTiros.clear();
            vectorBombas.clear();
            vectorInimigosTerrestres.clear();
            vectorInimigosAereos.clear();
            currentTime = 0;
            for (int i = 0; i < initialVectorInimigosTerrestres.size(); i++) {
                vectorInimigosTerrestres.push_back(initialVectorInimigosTerrestres[i]);
            }
            
            for (int i = 0; i < initialVectorInimigosAereos.size(); i++) {
                vectorInimigosAereos.push_back(initialVectorInimigosAereos[i]);
            }
            
            cout << "Jogo Reiniciado!" << endl;
            usleep(500000);
}

void Jogador::pontoTopo(){
    jogador.topoX =  jogador.centro_x - jogador.raio * sinf(jogador.ang*(2*M_PI)/360);
    jogador.topoY =  jogador.centro_y + jogador.raio * cosf(jogador.ang*(2*M_PI)/360);
    /*
    cout << "top X: " << jogador.topoX << endl;
    cout << "top Y: " << jogador.topoY << endl;
    cout << "centro X: " << jogador.centro_x << endl;
    cout << "centro Y: " << jogador.centro_y << endl;
    */
}

void InimigoAereo::TeleportaInimigo(){

        double copiaX;
        double copiaY;
        double inicialX;
        double inicialY;
        double finalX;
        double finalY;
        double copiaAng;
        int beenHereDoneThat=0;

        copiaX = this->centro_x;
        copiaY = this->centro_y;
        copiaAng = this->ang;


            while(true){
                //O inimigo está em estado de colisão, esse while o tira desse estado
                while(beenHereDoneThat==0){
                    copiaX = copiaX + 0.1 * sinf(copiaAng*(2*M_PI)/360);
                    copiaY = copiaY - 0.1 * cosf(copiaAng*(2*M_PI)/360);
                    if(vaiBater(copiaX,copiaY)==0){
                       // cout << "Ponto onde não bate mais!" << endl;
                       // cout << copiaX << " " << copiaY << endl;
                        beenHereDoneThat=1;
                        inicialX = copiaX;
                        inicialY = copiaY;
                    }
                }

                copiaX = copiaX + 0.1 * sinf(copiaAng*(2*M_PI)/360);
                copiaY = copiaY - 0.1 * cosf(copiaAng*(2*M_PI)/360);
                
                //aqui o inimigo colidiu de novo, queremos teleportar pra esse ponto
                if(vaiBater(copiaX,copiaY)==1){
                        //cout << "Segunda batida, atribuindo esses pontos ao Inimigo!" << endl;
                        //cout << copiaX << " " << copiaY << endl;
                    //porém, se a dist for muito curta, o avião pode ficar preso, se teleportando pra sempre
                    //por isso se o inimigo entrar com um ângulo muito próximo do angulo do círculo, causando
                    //esse problema, teleportamos para o centro da arena pra evitar travamentos
                    if(abs(inicialX - copiaX) < 15){
                        this->centro_x = arena.centro_x;
                        this->centro_y = arena.centro_y;
                        cout << "Contingency Enemy Teleport" << endl;
                        break;
                    } else {
                    this->centro_x = copiaX;
                    this->centro_y = copiaY;
                    break;
                    }
                }
            }
}

void InimigoAereo::RandomizaAng(GLdouble time){
    int r = (rand() % 100) + 1;

    //cout << r << endl;
    if (r==1){
        this->IncrementaAng(time);
    } else if (r==2){
        this->DecrementaAng(time);
    } else {
        //Do nothing
    }
}

void InimigoAereo::ContaTiros(GLdouble time){
    if(this->tirosDados <= this->freqTiro*time/1000){
        this->tirosDados = this->tirosDados+1;
    }
}

void InimigoAereo::Atira(GLdouble time){
    //cout << "freqTiros x Time: " << this->freqTiro*time << endl;
    if(this->tirosDados <= this->freqTiro*time/1000){
        Tiro t;
        t.raio = this->raio/6;
        t.centro_x = this->centro_x;
        t.centro_y = this->centro_y;
        t.ang = (this->ang);
        t.corR = 1;
        t.corG = 0;
        t.corB = 0;
        vectorTirosInimigos.push_back(t);
        this->tirosDados = this->tirosDados + 1;
    }

}

void idle(){

    currentTime = glutGet(GLUT_ELAPSED_TIME);
    timeDiference = currentTime - previousTime;
    previousTime = currentTime; 
    limpaTiros();
    limpaTirosInimigos();
    diminuiRaioBombas(currentTime);

    //cout << currentTime << endl;

    if(colisaoVoador==0)
        for (int i = 0; i < vectorInimigosAereos.size(); i++) {
                vectorInimigosAereos[i].Atira(currentTime);
                //Mantendo o vetor de backup atualizado
                initialVectorInimigosAereos[i].ContaTiros(currentTime);
            if(vaiBater(vectorInimigosAereos[i].centro_x,vectorInimigosAereos[i].centro_y)==0)
                vectorInimigosAereos[i].RandomizaAng(currentTime);
        }

    //cout << "velocidade: " << jogador.velocidade << endl;
    if (vectorTiros.size()>0 && vectorInimigosAereos.size()>0)
        inimigoMorto();
    
    if(colisaoVoador==0)
        if(vectorTiros.size()>0)
            moveTiro(velocidadeT*jogador.velocidade,timeDiference);

    if(colisaoVoador==0)
        if(vectorTirosInimigos.size()>0)
            moveTiroInimigo(velocidadeTiroInimigo+velocidadeInimigo,timeDiference);

    if(vectorBombas.size()>0)
        moveBomba(jogador.velocidade,timeDiference);

        if((keyStatus['r']==1 || keyStatus['R']==1)){
            restartGame();
        }

        if (decolando == 1){
            decola(currentTime);
        }
            if(colisaoVoador == 0){
                jogador.MoveHelice(jogador.velocidade,timeDiference);
                for (int i = 0; i < vectorInimigosAereos.size(); i++) {
                    vectorInimigosAereos[i].MoveHelice(vectorInimigosAereos[i].velocidade,timeDiference);
                }
            }
        if (vaiBater(jogador.centro_x,jogador.centro_y) == 1) {
            TeleporteJogador();
        }

        for (int i = 0; i < vectorInimigosAereos.size(); i++) {
            if (vaiBater(vectorInimigosAereos[i].centro_x,vectorInimigosAereos[i].centro_y)==1){
                vectorInimigosAereos[i].TeleportaInimigo();
            }
        }

        if(decolou == 1){
            if(colisaoVoador != 1)
                jogador.MoveJogador(jogador.velocidade,timeDiference);
        }
        if((keyStatus['a'] == 1 || keyStatus['A']==1) && decolou == 1 && colisaoVoador == 0){
            jogador.IncrementaAng(timeDiference);
        }
        if((keyStatus['d'] == 1 || keyStatus['D']==1) && decolou ==1 && colisaoVoador == 0){
            jogador.DecrementaAng(timeDiference);
        }
        if((keyStatus['+'] == 1 || keyStatus['='] == 1) && decolou ==1 && colisaoVoador == 0){
            jogador.IncrementaVel(timeDiference);
        }
        if((keyStatus['-'] == 1) && decolou ==1 && colisaoVoador == 0){
            jogador.DecrementaVel(timeDiference);
        }

        if(decolou == 1)
            colisaoInimigoVoador(jogador.centro_x,jogador.centro_y);

    jogador.anguloCanhao();
    jogador.pontoTopo();

        if(colisaoVoador==0)
            for (int i = 0; i < vectorInimigosAereos.size(); i++) {
                vectorInimigosAereos[i].MoveInimigo(velocidadeInimigo,timeDiference);
            }
        

	glutPostRedisplay();
}

void passive (int x, int y){
    passiveX = x;
    passiveY = altura-y;
}

void Jogador::anguloCanhao() {
    jogador.angCanhao = -45;
    jogador.angCanhao = jogador.angCanhao - atan2(passiveX - largura,passiveX - 0)*180/M_PI;
}



void mouse (int button, int state, int x, int y){

	clickX = x;
    clickY = altura - y;

    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && decolou == 1 && colisaoVoador != 1) {
        Tiro t;
        t.raio = jogador.raio/6;
        t.centro_x = jogador.topoX;
        t.centro_y = jogador.topoY;
        t.ang = (jogador.ang + jogador.angCanhao);
        t.corR = 0.8;
        t.corG = 0.8;
        t.corB = 0.8;
        vectorTiros.push_back(t);
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN && decolou == 1 && colisaoVoador != 1) {
        Bomba b;
        b.createdTime = glutGet(GLUT_ELAPSED_TIME);
        b.raio = jogador.raio/4;
        b.centro_x = jogador.centro_x;
        b.centro_y = jogador.centro_y;
        b.ang = (jogador.ang);
        b.corR = 0.5;
        b.corG = 0.5;
        b.corB = 0.5;
        vectorBombas.push_back(b);
    }
        glutPostRedisplay();
}

int main(int argc, char** argv){
    read_xml(argv[1]);
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(altura,largura);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Plane War");
    init();
    glutIdleFunc(idle);
    glutDisplayFunc(display);
    glutKeyboardUpFunc(keyUp);
    glutKeyboardFunc(keyPress);
    glutPassiveMotionFunc(passive);
    glutMouseFunc(mouse);
    glutMainLoop();
    return 0;
}